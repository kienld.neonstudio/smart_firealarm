#include<xc.h>
#include<pic.h>
#include <stdio.h>
#include<stdlib.h>
#include "uart.h"
#include<string.h>
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = ON      // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOREN = ON       // Brown-out Reset Enable bit (BOR enabled)
#pragma config LVP = OFF        // Low-Voltage (Single-Supply) In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EEPROM Memory Code Protection bit (Data EEPROM code protection off)
#pragma config WRT = OFF        // Flash Program Memory Write Enable bits (Write protection off; all program memory may be written to by EECON control)
#pragma config CP = OFF         // Flash Program Memory Code Protection bit (Code protection off)

#define DHT11_Data_Pin   PORTBbits.RB1
#define DHT11_Data_Pin_Direction  TRISBbits.TRISB1
#define _XTAL_FREQ 8000000 //8 Mhz
#define API_WRITE_KEY		"C7JFHZY54GLCJY38"
char buffer[18];
char PhoneNumber[15];
char AdPhone[15]="+84832386164";
volatile int a=0;
volatile int b=0;
volatile int c=0;
volatile int  stt=0;  
volatile int    _CheckN=0;
volatile int    _CheckS=0;

void Init();
void Send_SMS(char *s);
void Call(char *s);

unsigned char Check_bit, Temp_byte_1, Temp_byte_2, RH_byte_1, RH_byte_2;
unsigned char  Sumation ;
char Temperature[];
char Humidity[];
void dht11_init();
void find_response();
char read_dht11();

void main()

{
    Init();
   // Send_SMS("He thong da duoc khoi dong");
    Call(AdPhone);
    int cnt=0;
    RCIF=0;
    __delay_ms(5000);
 
int _WR=0;
while(1)
{   //Call(AdPhone);
   INTCONbits.GIE=1; 
   INTCONbits.PEIE=1;  
   PIE1bits.RCIE=1; 
   __delay_ms(800);
    dht11_init();
    find_response();
   
    // phat hien chay qua nhiet do 
     if(Check_bit == 1){
        RH_byte_1 = read_dht11();
        RH_byte_2 = read_dht11();
        Temp_byte_1 = read_dht11();
        Temp_byte_2 = read_dht11();
        Sumation = read_dht11();
        
        if(Sumation == ((RH_byte_1+RH_byte_2+Temp_byte_1+Temp_byte_2) & 0XFF)){
           
            
            if(Temp_byte_1 > 30){
                RD1 = 1;
                RD2 = 1;
                __delay_ms(500); // ?? c�i n?a gi�y r?i t?t cho ?? ?n
                RD1 = 0;
                RD2 = 0; 
            };
            
               UART_Write_Text("AT+CIPMODE=0");
               __delay_ms(1000);
                UART_Write_Text("AT+CIPMUX=0");
                 __delay_ms(1000);
                UART_Write_Text("AT+CGATT=1"); /* Open GPRS context */
                 __delay_ms(1000);
                UART_Write_Text("AT+CSTT=\"internet\""); /* Query the GPRS context */
                 __delay_ms(1000);
                UART_Write_Text("AT+CIICR");
                 __delay_ms(1000);
                UART_Write_Text("AT+CIFSR");
                 __delay_ms(1000);
                UART_Write_Text("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",\"80\"");
                 __delay_ms(1000);
                UART_Write_Text("AT+CIPSEND");
                   __delay_ms(1000);
               UART_Write_Text("GET https://api.thingspeak.com/update?api_key=0HNIYBS21W9QLE8S&field1=" + Temp_byte_1);
               UART_Write((char)26);
                            
        }
    }

    // Phat hien chay bang khoi
   if (RA5==1 && _WR < 2 )
   {
       
       Send_SMS("Co chay xay ra tai phong 103-G2");
       __delay_ms(2000);
       Call(AdPhone);
       _WR++; 
        RD1=1;
        RD2=1;
   }
   if(RA5==0) 
   {
       RD1=0;
       RD2=0;
   }
    if(stt==1)
    {
        if (strcmp(PhoneNumber,AdPhone)==0)
        {
            // Tat he thong
            if (strcmp(buffer,"off")==0)
            {
                RD0=0;
                RD1=0;
                RD2=0;
                
            }
            
            // Bat he thong he thong
            if(strcmp(buffer,"on")==0)
            {
                RD0=1;
                //RD1=1;
                //RD2=1;
                _WR=0;
                __delay_ms(5000);
                Send_SMS("He thong duoc bat");                
            }
            // Kiem tra he thong bao dong]
            if(strcmp(buffer,"check")==0)
            {
                RD1=1;
                RD2=1;
                __delay_ms(5000);
                Send_SMS("He thong bao dong bang tin nhan hoat dong binh thuong");                
            }
            // Thay doi so dien thoai
            if(buffer[0]=='c' && buffer[1]=='h' && buffer[2]=='n' )
            {
                memset(AdPhone,NULL, strlen(AdPhone));
                int i=-1;
                for (int j=3;j<strlen(buffer); j++)
                {
                    i++;
                    AdPhone[i]=buffer[j];                   
                }
                
            }
        }
        __delay_ms(1000);
        stt=0;
        _CheckN=0;
        _CheckS=0;
        for (int j=0;j<a;j++)
        {
            buffer[j]=NULL;
        }
        a=0;
        for (int j=0;j<b;j++)
        {
            PhoneNumber[j]=NULL;
        }
        b=0;
       
       
    } 
 } 
}


 void __interrupt() ISR(void)
{
    
    if(RCIF)
    {
        char s = RCREG;
     // Lay ra tin nhan
        if(_CheckS==2 && s==0x0D)
        {
            _CheckS+=1;
            stt=1;
        }
        if (_CheckS==2)
        {
            buffer[a]=s;
            a++;
        }
        if (s==0X0A)
        {
            _CheckS+=1;
        }
      
     //Lay ra so dien thoai
        if (_CheckN==1 && s=='"')
        {
            _CheckN+=1;
        }
        
        if (_CheckN==1)
        {
            PhoneNumber[b]=s;
            b++;
        }
        if(s=='"')
        {
            _CheckN+=1;
        }
        
            
    if(RCSTAbits.OERR)  
        {           
            CREN = 0;
            NOP();
            CREN=1;
        }
    }
}
 
void Init()
{
    a=0;
    b=0;
    ADCON1 |= 0x07; 
    PCFG3 = 0;
    PCFG2=1;
    PCFG1=1;
    PCFG0=1;
	TRISC=0x80;	
    TRISA=0xFF;
	RA5=0;
	TRISD=0x00;
	RD4=0;
	RD3=0;
	RD5=0;
    RD1=0;
    RD2=0;
    RD0=1;
	
    UART_Init(9600);
	__delay_ms(2000);
	RD4=0;
    RD4=1;
    __delay_ms(500);
    RD4=0;
   __delay_ms(10000);
	UART_Write_Text("AT");
    UART_Write(0x0D);
    __delay_ms(500);		//delay 0.5s
    UART_Write_Text("AT");
    UART_Write(0x0D);
    __delay_ms(500);
    UART_Write_Text("ATE0");
    UART_Write(0x0D);
    __delay_ms(500);
    UART_Write_Text("AT+CMGF=1");
    UART_Write(0x0D);
	__delay_ms(500);
	UART_Write_Text("AT+CNMI=2,2,0,0,0");
     UART_Write(0x0D);
    __delay_ms(1000);
    
   UART_Write_Text("AT+CMGDA=");
	UART_Write(0x22);
	UART_Write_Text("DEL ALL");
	UART_Write(0x22);
    UART_Write(0x0D);
     __delay_ms(1000);
     
}

void Send_SMS(char *s)
{
      
    UART_Write_Text("AT+CMGS=");
	UART_Write(0x22);
	UART_Write_Text(AdPhone);
	UART_Write(0x22);
	UART_Write(0x0D); 
   __delay_ms(1000);
	UART_Write_Text(s); 
    UART_Write(0x0D); 
	UART_Write(0x1A);
    __delay_ms(2000);
}
void Call(char *s)
{
    
    UART_Write_Text("ATD");
    UART_Write_Text(s);
    UART_Write(0x3B);
    UART_Write(0x0D);
    __delay_ms(20000);
}


void dht11_init(){
 DHT11_Data_Pin_Direction= 0; //Configure RD0 as output
 DHT11_Data_Pin = 0; //RD0 sends 0 to the sensor
 __delay_ms(18);
 DHT11_Data_Pin = 1; //RD0 sends 1 to the sensor
 __delay_us(30);
 DHT11_Data_Pin_Direction = 1; //Configure RD0 as input
 }

/*
 * This will find the dht22 sensor is working or not.
 */

 void find_response(){
 Check_bit = 0;
 __delay_us(40);
 if (DHT11_Data_Pin == 0){
 __delay_us(80);
 if (DHT11_Data_Pin == 1){
    Check_bit = 1;
 }     
 __delay_us(50);}
 }
 
 /*
 This Function is for read dht22.
 */
 
 char read_dht11(){
 char data, for_count;
 for(for_count = 0; for_count < 8; for_count++){
     while(!DHT11_Data_Pin); 
    __delay_us(30);
    if(DHT11_Data_Pin == 0){
        data&= ~(1<<(7 - for_count)); //Clear bit (7-b)
    }
    else{
        data|= (1 << (7 - for_count)); //Set bit (7-b)
        while(DHT11_Data_Pin);
    } //Wait until PORTD.F0 goes LOW
    }
 return data;
 }